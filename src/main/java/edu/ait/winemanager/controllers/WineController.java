package edu.ait.winemanager.controllers;

import edu.ait.winemanager.repositories.IWineSummary;
import edu.ait.winemanager.dto.Wine;
import edu.ait.winemanager.exceptions.WineNotFoundException;
import edu.ait.winemanager.repositories.IWineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;



import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class WineController {

    @Autowired
    IWineRepository wineRepository;

    @GetMapping("/wines")
    public Page<Wine> getWinesByPage(Pageable pageable) {

        Page<Wine>winePage = wineRepository.findAll(pageable);
        return winePage;
    }

    @GetMapping("/wines/summary")
    public List<IWineSummary> getAllWineSummaries(Pageable pageable) {

        return wineRepository.findAllWineSummaryBy(pageable);

    }

    @GetMapping("wines/{id}")
    public Optional<Wine> getWineById(@PathVariable int id){

        return wineRepository.findById(id);

    }
    @DeleteMapping("wines/{id}")
    public void deleteWineById(@PathVariable int id){
        try {
            wineRepository.deleteById(id);
        }
        catch (EmptyResultDataAccessException e){
            throw new WineNotFoundException("Unable to delete wine with id:" + id);
        }
    }

    @PostMapping("wines/")
    public ResponseEntity createWine(@RequestBody Wine newWine){

        //set id to null
        newWine.setId(null);
        //add the wine
        Wine savedWine = wineRepository.save(newWine);

        URI location = ServletUriComponentsBuilder
                .fromCurrentRequest().path("{id}")
                .buildAndExpand(newWine.getId()).toUri();
        return ResponseEntity.created(location).build();

    }
    @PutMapping("wines/")
    public ResponseEntity updateWine(@RequestBody Wine newWine) {

        //capture the ID passed in with the newWine
        Integer newWineId = newWine.getId();
        //Create the new wine
        Wine savedWine = wineRepository.save(newWine);
        //If the savedWine has the same Id as the newWind ID we captured earlier
        //This is an update. Take into account that the new ID might be null
        if(savedWine.getId().equals(newWineId)){
            return ResponseEntity.status(HttpStatus.OK).build();
        }

        else
        {
            URI location = ServletUriComponentsBuilder
                    .fromCurrentRequest().path("{id}")
                    .buildAndExpand(newWine.getId()).toUri();
            return ResponseEntity.created(location).build();
        }

    }
}
