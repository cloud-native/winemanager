package edu.ait.winemanager.repositories;


import edu.ait.winemanager.dto.Wine;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IWineRepository extends JpaRepository<Wine, Integer>{

    List<IWineSummary> findAllWineSummaryBy(Pageable pageable);
}
