package edu.ait.winemanager.repositories;

public interface IWineSummary {

    Integer getId();

    String getName();

    int getYear();


}
