package edu.ait.winemanager;

import edu.ait.winemanager.dao.WineDAO;

public class WineDAOTest {

    public static void main(String[] args) {
        WineDAO wineDAO = new WineDAO();
        System.out.println("All Wines: " + wineDAO.findAll());
        System.out.println("Wine with ID 3:" + wineDAO.findById(3));
    }
}
